package mypackage;


public class Fraction {

    public static boolean DISPLAY_FRACTION = true;

    private int denom, div;

    public Fraction(int denom, int div) throws Exception {

        this.denom = denom;
        this.div = div;

        if(div == 0) throw new Exception("Invalid fraction : division by zero");

        trim();
    }

    public Fraction(String string) throws Exception {

        try {

            if(string.contains("/")) {

                String s[] = string.split("/");

                if(s.length != 2) throw new Exception("Invalid fraction : " + string);

                denom = Integer.parseInt(s[0]);
                div =   Integer.parseInt(s[1]);
            }

            else {

                denom = Integer.parseInt(string);
                div = 1;
            }

            if(div == 0) throw new Exception("Invalid fraction : division by zero");

            trim();
        }

        catch (NumberFormatException n_err) {

            throw new Exception("Invalid number format : " + string);
        }
    }

    public void set(int denom, int div) {

        this.denom = denom;
        this.div = div;
    }

    public Fraction invert() throws Exception {

        return new Fraction(div, denom);
    }

    public Fraction plus(Fraction m) throws Exception {

        return new Fraction(denom * m.getDiv() + m.getDenom() * div, div * m.getDiv());
    }

    public Fraction minus(Fraction m) throws Exception {

        return new Fraction(denom * m.getDiv() - m.getDenom() * div, div * m.getDiv());
    }

    public Fraction multiply(Fraction m) throws Exception {

        return new Fraction(denom * m.getDenom(), div * m.getDiv());
    }

    public Fraction divide(Fraction m) throws Exception {

        return new Fraction(denom * m.getDiv(), div * m.getDenom());
    }

    public void trim() {

        if(div < 0 && denom < 0) {

            div = Math.abs(div);
            denom = Math.abs(denom);
        }

        if(div < 0 && denom >= 0) {

            div = Math.abs(div);
            denom *= -1;
        }

        for(int i = 2; i <= Math.max(Math.abs(div), Math.abs(denom)); i ++) {

            if(Math.abs(div) % i == 0 && Math.abs(denom) % i == 0) {

                div /= i;
                denom /= i;

                i = i - 1;

                continue;
            }
        }
    }

    @Override
    public String toString() {

        trim();

        if(DISPLAY_FRACTION) {

            if(div == 1 || denom == 0) return Integer.toString(denom);

            else return new String(denom + "/" + div);
        }

        else return Double.toString((double)denom / (double)div);
    }

    public int getDenom() {
        return denom;
    }

    public void setDenom(int denom) {
        this.denom = denom;
    }

    public int getDiv() {
        return div;
    }

    public void setDiv(int div) {
        this.div = div;
    }
}
