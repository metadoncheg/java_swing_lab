package mypackage;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class Result extends JFrame{

    private View owner;

    JTable result;

    int m_width, m_height;

    public Result(View owner){

        this.owner = owner;

        this.setTitle("Result Matrix");

        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

        m_width  = owner.getmWidth();
        m_height = owner.getmHeight_2();

        result = new JTable(m_width, m_height);

        this.add(result);
        this.pack();
        this.setVisible(false);
    }

    public void prepare_and_show(Fraction[][] m) {

        m_width  = m[0].length;
        m_height = m.length;

        DefaultTableModel model = (DefaultTableModel)result.getModel();

        while(model.getRowCount() != 0) model.removeRow(0);

        model.setColumnCount(m_width);
        model.setRowCount(m_height);

        for(int i = 0; i < model.getRowCount(); i ++) {
            for(int j = 0; j < model.getColumnCount(); j ++) {

                result.setValueAt(m[i][j], i, j);
            }
        }

        this.setVisible(true);

        this.pack();
    }


}
