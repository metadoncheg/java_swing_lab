package mypackage;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Settings extends JFrame {

    private View owner;

    private JSpinner width_1, height_1, width_2, height_2;

    public Settings(final View owner){

        this.owner = owner;

        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        SpinnerNumberModel wmodel_1 = new SpinnerNumberModel(3, 2, 16, 1);
        SpinnerNumberModel hmodel_1 = new SpinnerNumberModel(3, 2, 16, 1);

        SpinnerNumberModel wmodel_2 = new SpinnerNumberModel(3, 2, 16, 1);
        SpinnerNumberModel hmodel_2 = new SpinnerNumberModel(3, 2, 16, 1);

        height_1 = new JSpinner(hmodel_1);
        width_1 = new JSpinner(wmodel_1);

        height_2 = new JSpinner(hmodel_2);
        width_2 = new JSpinner(wmodel_2);

        JPanel wpanel = new JPanel();

          wpanel.setLayout(new BoxLayout(wpanel, BoxLayout.X_AXIS));

          wpanel.add(new JLabel("Enter width for first matrix"));
          wpanel.add(width_1);

        JPanel hpanel = new JPanel();

          hpanel.setLayout(new BoxLayout(hpanel, BoxLayout.X_AXIS));

          hpanel.add(new JLabel("Enter height for first matrix"));
          hpanel.add(height_1);

        JPanel wpanel_2 = new JPanel();

          wpanel_2.setLayout(new BoxLayout(wpanel_2, BoxLayout.X_AXIS));

          wpanel_2.add(new JLabel("Enter width for second matrix"));
          wpanel_2.add(width_2);

        JPanel hpanel_2 = new JPanel();

          hpanel_2.setLayout(new BoxLayout(hpanel_2, BoxLayout.X_AXIS));

          hpanel_2.add(new JLabel("Enter height for second matrix"));
          hpanel_2.add(height_2);

        final JButton btn = new JButton("Yeah");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Integer value;

                value = (Integer)width_1.getValue();

                owner.setmWidth(value.intValue());

                value = (Integer)height_1.getValue();

                owner.setmHeight(value.intValue());

                value = (Integer)width_2.getValue();

                owner.setmWidth_2(value.intValue());

                value = (Integer)height_2.getValue();

                owner.setmHeight_2(value.intValue());

                if(isValidMultiplyMatrix()){

                    owner.changeTable(owner.table, owner.getmWidth(),owner.getmHeight());
                    owner.changeTable(owner.table_2, owner.getmWidth_2(),owner.getmHeight_2());

                    setVisible(false);

                }
                else {

                    JOptionPane.showMessageDialog(null, new String("Invalid size"));

                }
            }
        });

        this.add(hpanel);
        this.add(wpanel);

        this.add(hpanel_2);
        this.add(wpanel_2);

        this.setTitle("Size of matrix");

        this.add(btn);

        this.pack();

        this.setVisible(false);
    }

    public boolean isValidMultiplyMatrix(){

        if(owner.getmWidth() == owner.getmHeight_2()){

            return true;
        }

        else return false;
    }

}
