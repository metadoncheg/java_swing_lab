package mypackage;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class View extends JFrame {

    public static enum Type{Multiplication, Invert, Determinant}

    private Type type;

    private int mWidth, mHeight, mWidth_2, mHeight_2;

    private Model model;

    Settings sizesettings;

    Result resultmatrix;

    JTable table, table_2;

    JLabel label_1, label_2;

    JButton btn_result;

    public View(Model model){

        this.model = model;

        type = Type.Multiplication;

        mWidth = mWidth_2 = 3;
        mHeight = mHeight_2 = 3;

        sizesettings = new Settings(this);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setTitle("Main window");

        table = new JTable(mWidth, mHeight);
        table_2 = new JTable(mWidth_2, mHeight_2);

        label_1 = new JLabel("First Matrix =" + " ");
        label_2 = new JLabel("Second Matrix =" + " ");

        this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));

        this.add(label_1);
        this.add(table);
        this.add(label_2);
        this.add(table_2);

        JMenuBar menubar = new JMenuBar();

        JMenu menu = new JMenu("Menu");

        menubar.add(menu);

        JMenuItem setting_menuItem = new JMenuItem("Settings");
        JMenuItem invert_menuItem = new JMenuItem("Invert");
        JMenuItem determinant_menuItem = new JMenuItem("Determinant");
        JMenuItem multiply_menuItem = new JMenuItem("Multiply");

        menu.add(multiply_menuItem);
        menu.add(invert_menuItem);
        menu.add(determinant_menuItem);
        menu.add(setting_menuItem);

        btn_result = new JButton("Multiply!");

        setting_menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               sizesettings.setVisible(true);
            }
        });

        invert_menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                label_1.setText("Matrix=");
                table_2.setVisible(false);
                label_2.setVisible(false);
                type = Type.Invert;
                btn_result.setText("Invert!");

            }
        });

        determinant_menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                label_1.setText("Matrix=");
                table_2.setVisible(false);
                label_2.setVisible(false);
                type = Type.Determinant;
                btn_result.setText("Count up!");

            }
        });

        multiply_menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                label_1.setText("FirstMatrix=");
                type = Type.Multiplication;
                table_2.setVisible(true);
                label_2.setVisible(true);
                btn_result.setText("Multiply!");
            }
        });

        this.add(btn_result);

        resultmatrix = new Result(this);

        btn_result.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                switch (type) {

                    case Multiplication :

                        try {

                            getModel().setL(extract(table));
                            getModel().setR(extract(table_2));

                            resultmatrix.prepare_and_show(getModel().multiply_matrix());
                        }

                        catch (Exception err) {

                            JOptionPane.showMessageDialog(null, err.getMessage(), "Error!",

                                    JOptionPane.ERROR_MESSAGE);

                            return;
                        }

                        return;

                    case Invert :

                        try {

                            Fraction[][] inv_result;

                            getModel().setL(extract(table));

                            inv_result = getModel().invert_matrix();

                            resultmatrix.prepare_and_show(inv_result);

                            return;
                        }

                        catch (Exception err) {

                            JOptionPane.showMessageDialog(null, err.getMessage(), "Error!",

                                    JOptionPane.ERROR_MESSAGE);

                            return;
                        }

                    case Determinant :

                        try {

                            getModel().setL(extract(table));

                            Fraction det_result = getModel().determinate();

                            JOptionPane.showMessageDialog(null, det_result,

                                    "Determinate", JOptionPane.INFORMATION_MESSAGE);

                            return;
                        }

                        catch (Exception err) {

                            JOptionPane.showMessageDialog(null, err.getMessage(), "Error!",

                                    JOptionPane.ERROR_MESSAGE);
                        }

                    default :

                        return;
                }
            }
        });

        this.setJMenuBar(menubar);

        this.pack();
    }

    public void changeTable(JTable tab, int width, int height){

        DefaultTableModel model = (DefaultTableModel)tab.getModel();

        while(model.getRowCount() != 0) model.removeRow(0);

        model.setColumnCount(width);
        model.setRowCount(height);

        for(int i = 0; i < model.getRowCount(); i ++) {
            for(int j = 0; j < model.getColumnCount(); j ++) {

                tab.setValueAt(j + ", " + i, i, j);
            }
        }
    }

    public Fraction[][] extract(JTable table) throws Exception {

        Fraction m[][] = new Fraction[table.getRowCount()][table.getColumnCount()];

        for(int i = 0; i < table.getRowCount(); i ++)
            for(int j = 0; j < table.getColumnCount(); j ++) {

                System.out.println(table.getValueAt(i, j));

                m[i][j] = new Fraction((String)table.getValueAt(i, j));
            }

        return m;
    }

    public int getmWidth() {
        return mWidth;
    }

    public void setmWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    public int getmHeight() {
        return mHeight;
    }

    public void setmHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public int getmWidth_2() {
        return mWidth_2;
    }

    public void setmWidth_2(int mWidth_2) {
        this.mWidth_2 = mWidth_2;
    }

    public int getmHeight_2() {
        return mHeight_2;
    }

    public void setmHeight_2(int mHeight_2) {
        this.mHeight_2 = mHeight_2;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
