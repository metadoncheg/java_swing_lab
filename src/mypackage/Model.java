package mypackage;

public class Model {

    private Fraction l[][], r[][];

    public Model() {

    }

    private void swap(Fraction[] src, Fraction[] dst) {

        Fraction[] temp = src;

        src = dst;

        dst = temp;
    }

    public Fraction[][] multiply_matrix() {

      /*  for(int i = 0; i < l.length; i ++) {

            for(int j = 0; j < l[i].length; j ++) {

                System.out.print(l[i][j] + " ");
            }

            System.out.println();
        }
        */

        Fraction f_arr[][] = new Fraction[l.length][r[0].length];

        for(int i = 0; i < l.length; i++)
            for(int j = 0; j < r[0].length; j++) {

                Fraction summary = new Fraction(0, 1);

                for(int k = 0; k < r.length; k ++) {

                    summary = summary.plus(l[i][k].multiply(r[k][j]));
                }

                f_arr[i][j] = summary;
            }

        return f_arr;
    }

    public Fraction[][] invert_matrix() throws Exception {

        Fraction m[][];
        Fraction m_invert[][] = new Fraction[l.length][l[0].length];

        m = l.clone();

        for(int i = 0; i < l.length; i ++)

            for(int j = 0; j < l.length; j ++) {

                if(i == j) m_invert[i][j] = new Fraction(1, 1);

                else m_invert[i][j] = new Fraction(0, 1);
            }

        int counter = 0;

        for(int j = 0; j < m.length - 1; j ++) {

            for(int i = j + 1; i < m.length; i ++) {

                if(m[j][counter].getDenom() == 0) {

                    for(int next = i + 1; next < m.length; next ++) {

                        if(m[next][counter].getDenom() != 0)
                        {
                            swap(m[j], m[next]);

                            swap(m_invert[j], m_invert[next]);

                            break;
                        }
                    }
                }

                if(m[j][counter].getDenom() == 0) throw new Exception("Can not apply Gauss method : zero columns found");

                Fraction scale =  m[i][counter].divide(m[j][counter]);

                for(int k = 0; k < m[j].length; k ++) {

                    m[i][k] = m[i][k].minus(m[j][k].multiply(scale));

                    m_invert[i][k] = m_invert[i][k].minus(m_invert[j][k].multiply(scale));
                }
            }

            counter ++;
        }

        counter = m.length - 1;

        for(int j = m.length - 1; j > 0; j --) {

            for(int i = j - 1; i >= 0; i --) {

                Fraction scale = m[i][counter].divide(m[j][counter]);

                for(int k = 0; k < m.length; k ++) {

                    m[i][k] = m[i][k].minus(m[j][k].multiply(scale));

                    m_invert[i][k] = m_invert[i][k].minus(m_invert[j][k].multiply(scale));
                }
            }

            counter --;
        }

        for(int i = 0; i < m.length; i ++) {

            Fraction scale = m[i][i].invert();

            for(int j = 0; j < m[i].length; j ++) {

                m[i][j] = m[i][j].multiply(scale);

                m_invert[i][j] = m_invert[i][j].multiply(scale);
            }
        }

        return m_invert;
    }

    public Fraction determinate() throws Exception {

        Fraction m[][];

        m = l.clone();

        Fraction result = new Fraction(1, 1);

        int counter = 0;

        for(int j = 0; j < m.length - 1; j ++) {

            for(int i = j + 1; i < m.length; i ++) {

                if(m[j][counter].getDenom() == 0) {

                    for(int next = i + 1; next < m.length; next ++) {

                        if(m[next][counter].getDenom() != 0)
                        {
                            swap(m[j], m[next]);

                            break;
                        }
                    }
                }

                if(m[j][counter].getDenom() == 0) throw new Exception("Can not find Det : zero columns found");

                Fraction scale =  m[i][counter].divide(m[j][counter]);

                for(int k = 0; k < m[j].length; k ++) {

                    m[i][k] = m[i][k].minus(m[j][k].multiply(scale));
                }
            }
            counter ++;
        }

        boolean has_null = false;

        for(int i = 0; i < m.length; i++){

            boolean string_null = true;

            for(int j = 0; j < m.length; j++){

                if(m[i][j].getDenom() != 0){

                    string_null = false;

                    break;
                }
            }

            if(string_null){

                has_null = true;
                break;
            }
        }

        if(has_null) return null;

        for(int i = 0; i < m.length; i++){

            result = m[i][i].multiply(result);
        }

        return result;
    }

    public Fraction[][] getL() {
        return l;
    }

    public void setL(Fraction[][] l) {
        this.l = l;
    }

    public Fraction[][] getR() {
        return r;
    }

    public void setR(Fraction[][] r) {
        this.r = r;
    }
}
